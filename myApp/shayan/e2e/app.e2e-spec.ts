import { ShayanPage } from './app.po';

describe('shayan App', function() {
  let page: ShayanPage;

  beforeEach(() => {
    page = new ShayanPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
