import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers', 
  //templateUrl: './servers.component.html',
  templateUrl: './servers.component.html',
  
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer :boolean = false ; 
  serverCreationStatus : string = "سرور ایجاد نشده " ; 
  serverName :string = 'سرور شماره 1 '; 
  serverCreation: boolean = false;
  constructor() { 
    setTimeout(()=>{
        this.allowNewServer = true
      },2000) ; 

  }

  ngOnInit() {
  }
  OnCreateServer(){
    this.serverCreation = true;
    this.serverCreationStatus ="سرور جدید به حول قوه الاهی ایجاد گردید   : "+ this.serverName
  }
  onUpdateServerName(arg1:Event){
    this.serverName = (<HTMLInputElement>arg1.target).value ; 

  }
}
